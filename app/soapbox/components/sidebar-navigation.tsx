import React from 'react';
import { defineMessages, FormattedMessage, useIntl } from 'react-intl';

import { Stack } from 'soapbox/components/ui';
import { useStatContext } from 'soapbox/contexts/stat-context';
import ComposeButton from 'soapbox/features/ui/components/compose-button';
import { useAppSelector, useGroupsPath, useFeatures, useOwnAccount, useSettings } from 'soapbox/hooks';

import DropdownMenu, { Menu } from './dropdown-menu';
import SidebarNavigationLink from './sidebar-navigation-link';

const messages = defineMessages({
  follow_requests: { id: 'navigation_bar.follow_requests', defaultMessage: 'Follow requests' },
  bookmarks: { id: 'column.bookmarks', defaultMessage: 'Bookmarks' },
  lists: { id: 'column.lists', defaultMessage: 'Lists' },
  events: { id: 'column.events', defaultMessage: 'Events' },
  developers: { id: 'navigation.developers', defaultMessage: 'Developers' },
  profile_directory: { id: 'navigation_bar.profile_directory', defaultMessage: 'Profile Directory' },
  blocks: { id: 'navigation_bar.blocks', defaultMessage: 'Blocks' },
  mutes: { id: 'navigation_bar.mutes', defaultMessage: 'Mutes' },
  filters: { id: 'navigation_bar.filters', defaultMessage: 'Filters' },
  domain_blocks: { id: 'navigation_bar.domain_blocks', defaultMessage: 'Domain blocks' },
  import_data: { id: 'navigation_bar.import_data', defaultMessage: 'Import' },
  config: { id: 'navigation_bar.soapbox_config', defaultMessage: 'Config' },
  logout: { id: 'navigation_bar.logout', defaultMessage: 'Logout' },
});

/** Desktop sidebar with links to different views in the app. */
const SidebarNavigation = () => {
  const intl = useIntl();
  const { unreadChatsCount } = useStatContext();

  const features = useFeatures();
  const settings = useSettings();
  const { account } = useOwnAccount();
  const groupsPath = useGroupsPath();

  const notificationCount = useAppSelector((state) => state.notifications.unread);
  const followRequestsCount = useAppSelector((state) => state.user_lists.follow_requests.items.count());
  const dashboardCount = useAppSelector((state) => state.admin.openReports.count() + state.admin.awaitingApproval.count());

  const makeMenu = (): Menu => {
    const menu: Menu = [];

    if (account) {

      menu.push({
        to: '/directory',
        text: intl.formatMessage(messages.profile_directory),
        icon: require('@tabler/icons/users.svg'),
      });

      if (account.locked || followRequestsCount > 0) {
        menu.push({
          to: '/follow_requests',
          text: intl.formatMessage(messages.follow_requests),
          icon: require('@tabler/icons/user-plus.svg'),
          count: followRequestsCount,
        });
      }

      if (features.bookmarks) {
        menu.push({
          to: '/bookmarks',
          text: intl.formatMessage(messages.bookmarks),
          icon: require('@tabler/icons/bookmark.svg'),
        });
      }

      if (features.lists) {
        menu.push({
          to: '/lists',
          text: intl.formatMessage(messages.lists),
          icon: require('@tabler/icons/list.svg'),
        });
      }

      if (features.events) {
        menu.push({
          to: '/events',
          text: intl.formatMessage(messages.events),
          icon: require('@tabler/icons/calendar-event.svg'),
        });
      }

      menu.push({
        to: '/blocks',
        text: intl.formatMessage(messages.blocks),
        icon: require('@tabler/icons/ban.svg'),
      });

      menu.push({
        to: '/mutes',
        text: intl.formatMessage(messages.mutes),
        icon: require('@tabler/icons/circle-x.svg'),
      });

      if (features.filters) {
        menu.push({
          to: '/filters',
          text: intl.formatMessage(messages.filters),
          icon: require('@tabler/icons/filter.svg'),
        });
      }

      if (features.federating) {
        menu.push({
          to: '/domain_blocks',
          text: intl.formatMessage(messages.domain_blocks),
          icon: require('@tabler/icons/circle-off.svg'),
        });
      }

      if (account.admin) {
        menu.push({
          to: '/soapbox/config',
          text: intl.formatMessage(messages.config),
          icon: require('@tabler/icons/components.svg'),
        });
      }

      if (features.import) {
        menu.push({
          to: '/settings/import',
          text: intl.formatMessage(messages.import_data),
          icon: require('@tabler/icons/cloud-upload.svg'),
        });
      }

      if (settings.get('isDeveloper')) {
        menu.push({
          to: '/developers',
          icon: require('@tabler/icons/code.svg'),
          text: intl.formatMessage(messages.developers),
        });
      }
      menu.push({
        to: '/logout',
        text: intl.formatMessage(messages.logout),
        icon: require('@tabler/icons/logout.svg'),
      });
    }

    return menu;
  };

  const menu = makeMenu();

  const makeFurriverse = (): Menu => {
    const menu: Menu = [];

    if (account) {
      menu.push({
        to: '/timeline/local',
        text: 'Local',
        icon: require('@tabler/icons/affiliate.svg'),
      });
      menu.push({
        to: '/timeline/animal.business',
        text: 'animal.business',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/blimps.xyz',
        text: 'blimps.xyz',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/cyberfurz.social',
        text: 'cyberfurz.social',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/floofy.tech',
        text: 'floofy.tech',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/furry.engineer',
        text: 'furry.engineer',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/fuzzy.zone',
        text: 'fuzzy.zone',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/gulp.cafe',
        text: 'gulp.cafe',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/meow.social',
        text: 'meow.social',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/pawb.fun',
        text: 'pawb.fun',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/pony.social',
        text: 'pony.social',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/red.fox.yt',
        text: 'red.fox.yt',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/redwombat.social',
        text: 'redwombat.social',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/shark.community',
        text: 'shark.community',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/snaggletooth.life',
        text: 'snaggletooth.life',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/squawk.avian.space',
        text: 'squawk.avian.space',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/thicc.horse',
        text: 'thicc.horse',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
      menu.push({
        to: '/timeline/tiggi.es',
        text: 'tiggi.es',
        icon: require('@tabler/icons/zoom-in.svg'),
      });
    }
    return menu;
  };

  const furriverse = makeFurriverse();

  return (
    <Stack space={4}>
      <Stack space={2}>
        <SidebarNavigationLink
          to='/'
          icon={require('@tabler/icons/home.svg')}
          text={<FormattedMessage id='tabs_bar.home' defaultMessage='Home' />}
        />

        {features.publicTimeline && (
          <>
            {furriverse.length > 0 && (
              <DropdownMenu items={furriverse}>
                <SidebarNavigationLink
                  icon={require('@tabler/icons/command.svg')}
                  text={<FormattedMessage id='tabs_bar.furriverse' defaultMessage='Furriverse' />}
                />
              </DropdownMenu>
            )}

            {features.federating && (
              <SidebarNavigationLink
                to='/timeline/fediverse'
                icon={require('@tabler/icons/topology-star-ring-3.svg')}
                text={<FormattedMessage id='tabs_bar.fediverse' defaultMessage='Fediverse' />}
              />
            )}
          </>
        )}

        <SidebarNavigationLink
          to='/search'
          icon={require('@tabler/icons/search.svg')}
          text={<FormattedMessage id='tabs_bar.search' defaultMessage='Search' />}
        />

        {account && (
          <>
            <SidebarNavigationLink
              to='/notifications'
              icon={require('@tabler/icons/bell.svg')}
              count={notificationCount}
              text={<FormattedMessage id='tabs_bar.notifications' defaultMessage='Notifications' />}
            />

            <SidebarNavigationLink
              to='/chats'
              icon={require('@tabler/icons/messages.svg')}
              count={unreadChatsCount}
              countMax={9}
              text={<FormattedMessage id='navigation.chats' defaultMessage='Chats' />}
            />

            <SidebarNavigationLink
              to='/messages'
              icon={require('@tabler/icons/mail.svg')}
              text={<FormattedMessage id='navigation.direct_messages' defaultMessage='Messages' />}
            />

            {features.groups && (
              <SidebarNavigationLink
                to={groupsPath}
                icon={require('@tabler/icons/circles.svg')}
                text={<FormattedMessage id='tabs_bar.groups' defaultMessage='Groups' />}
              />
            )}

            <SidebarNavigationLink
              to={`/@${account.acct}`}
              icon={require('@tabler/icons/user.svg')}
              text={<FormattedMessage id='tabs_bar.profile' defaultMessage='Profile' />}
            />

            <SidebarNavigationLink
              to='/settings'
              icon={require('@tabler/icons/settings.svg')}
              text={<FormattedMessage id='tabs_bar.settings' defaultMessage='Settings' />}
            />

            {account.staff && (
              <SidebarNavigationLink
                to='/soapbox/admin'
                icon={require('@tabler/icons/dashboard.svg')}
                count={dashboardCount}
                text={<FormattedMessage id='tabs_bar.dashboard' defaultMessage='Dashboard' />}
              />
            )}
          </>
        )}

        {menu.length > 0 && (
          <DropdownMenu items={menu} placement='top'>
            <SidebarNavigationLink
              icon={require('@tabler/icons/dots-circle-horizontal.svg')}
              text={<FormattedMessage id='tabs_bar.more' defaultMessage='More' />}
            />
          </DropdownMenu>
        )}
      </Stack>

      {account && (
        <ComposeButton />
      )}
    </Stack>
  );
};

export default SidebarNavigation;
